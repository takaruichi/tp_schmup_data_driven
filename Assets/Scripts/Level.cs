﻿using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level
{
    public LevelDescription Description;
    public float levelStartTime;

    private List<EnemyDescription> enemiesToSpawn;

    public void Load(LevelDescription levelDescription)
    {
        // Prepare a data structure to save which enemies you have/need to spawn (using
        // levelDescription)
        // Save the start time (using Time.time)
        enemiesToSpawn = new List<EnemyDescription>(levelDescription.Enemies);
        this.Description = levelDescription;
        //SceneManager.LoadScene(Description.Scene);
        levelStartTime = Time.time;
    }

    public void Execute()
    {
        // Check which enemies you have to spawn using the start time of your level
        // and you data struture.
        // Foreach (enemy in EnemySpawn) {
        // if (enemy.IsAlreadySpawn) continue;
        // else if (enemy.NeedToBeSpawned(levelStartTime))
        // Spawn(enemy);
        //}

        //EnemyDescription[] EnemySpawn = Description.Enemies;
        float timeSinceStart = Time.time - this.levelStartTime;
        /*foreach (EnemyDescription enemy in EnemySpawn)
        {
            if (enemy.IsAlreadySpawn) continue;
            else if (enemy.NeedToBeSpawned(levelStartTime))
                Spawn(enemy);

           // if (enemiesToSpawn[i].SpawnDate > timeSinceStart)
        }*/

        for (int i = 0; i < enemiesToSpawn.Count; ++i)
        {
            EnemyDescription enemyDescription = this.enemiesToSpawn[i];
            if (enemiesToSpawn[i].SpawnDate > timeSinceStart)
            {
                EnemyFactory.GetEnemy(enemyDescription.SpawnPosition, Quaternion.Euler(0.0f, 0.0f, 180.0f), enemyDescription.PrefabPath);
                enemiesToSpawn.RemoveAt(i);
                i--;
            }
        }
    }

    public void Spawn(EnemyDescription enemy)
    {
        EnemyFactory.GetEnemy(enemy.SpawnPosition, new Quaternion(0.0f,0.0f,180,0.0f), enemy.PrefabPath);
        enemy.IsAlreadySpawn = true;
    }

    public bool isFinished()
    {
        float timeSinceStart = Time.time - this.levelStartTime;
        if (timeSinceStart >= Description.Duration)
            return true;
        return false;
    }
}
