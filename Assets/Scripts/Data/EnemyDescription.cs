﻿// <copyright file="EnemyDescription.cs" company="AAllard">Copyright AAllard. All rights reserved.</copyright>

namespace Data
{
    using System.Xml.Serialization;
    using UnityEngine;

    [XmlRoot("EnemyDescription")]
    [XmlType("EnemyDescription")]
    public class EnemyDescription
    {
        [XmlElement]
        public float SpawnDate
        {
            get;
            private set;
        }

        [XmlElement]
        public Vector2 SpawnPosition
        {
            get;
            private set;
        }

        [XmlElement]
        public string PrefabPath
        {
            get;
            private set;
        }

        [SerializeField]
        private bool isAlreadySpawn = false;
        public bool IsAlreadySpawn
        {
            get;
            set;
        }

        public bool NeedToBeSpawned(float levelStartTime)
        {
            float timeSinceStart = Time.time - levelStartTime;
            if (SpawnDate > timeSinceStart)
            //if ((levelStartTime + SpawnDate) >= Time.time)
                return true;
            return false;
        }
    }
}