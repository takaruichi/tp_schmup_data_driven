﻿// <copyright file="GameManager.cs" company="1WeekEndStudio">Copyright 1WeekEndStudio. All rights reserved.</copyright>

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Xml.Serialization;
using Data;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject playerPrefab;
    
    [SerializeField]
    private float rateOfEnemySpawn = 0.2f;

    [SerializeField]
    private float maximumRateOfEnemySpawn = 1f;

    [SerializeField]
    private float rateOfEnemySpawnIncreaseStep = 0.02f;

    private double lastEnemySpawnTime;

    public List<LevelDescription> Data;
    public Level currentLevel;
    private int currentLevelIndex;

    public TextAsset text;

    public static GameManager Instance
    {
        get;
        private set;
    }

    public PlayerAvatar PlayerAvatar
    {
        get;
        private set;
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("There is multiple instance of singleton GameManager");
            return;
        }

        Instance = this;
        currentLevelIndex = -1;
    }

    private void Start()
    {
        // Spawn the player.
        GameObject player = (GameObject)GameObject.Instantiate(Instance.playerPrefab, new Vector3(0f, 0f), Quaternion.identity);
        this.PlayerAvatar = player.GetComponent<PlayerAvatar>();
        if (this.PlayerAvatar == null)
        {
            Debug.LogError("Can't retrieve the PlayerAvatar script.");
        }
        Data = XmlHelpers.DeserializeDatabaseFromXML<LevelDescription>(text);
        Debug.Log("LevelDescription loaded = " + Data.Count);

        StartNextLevel();
    }

    private void StartNextLevel()
    {
        /*if (currentLevel != null && currentLevel.Description.Duration + currentLevel.levelStartTime >= Time.time )
            StartNextLevel();*/
        this.currentLevelIndex++;
        // TODO: Get levelDescription currentLevelIndex
        // TODO: Instantiate a level with the description
        if (currentLevelIndex >= Data.Count)
            currentLevelIndex = 0;

        /*currentLevel = gameObject.AddComponent<Level>();
        currentLevel.Load(Data[currentLevelIndex]);
        //currentLevel = new Level(Data[currentLevelIndex]);
        currentLevel.Execute();*/
        LevelDescription levelDescription = Data[currentLevelIndex];

        this.currentLevel = new Level();
        this.currentLevel.Load(levelDescription);

    }

    private void Update()
    {
        //this.RandomSpawn();
        this.currentLevel.Execute();

        if (this.currentLevel.isFinished())
        {
            this.StartNextLevel();
        }
    }
    
    private void RandomSpawn()
    {
        if (this.rateOfEnemySpawn <= 0f)
        {
            return;
        }

        float durationBetweenTwoEnemySpawn = 1f / this.rateOfEnemySpawn;

        if (Time.time < this.lastEnemySpawnTime + durationBetweenTwoEnemySpawn)
        {
            // The bullet gun is in cooldown, it can't fire.
            return;
        }

        // Spawn an enemy.
        string prefabPath = "Prefabs/Enemy_01";
        if (Random.value < 0.2f)
        {
            prefabPath = "Prefabs/Enemy_02";
        }

        float randomY = Random.Range(-4f, 4f);
        EnemyFactory.GetEnemy(new Vector3(10f, randomY), Quaternion.Euler(0f, 0f, 180f), prefabPath);
        this.lastEnemySpawnTime = Time.time;

        // Up the difficulty.
        this.rateOfEnemySpawn += this.rateOfEnemySpawn > this.maximumRateOfEnemySpawn ? 0f : this.rateOfEnemySpawnIncreaseStep;
    }
}
